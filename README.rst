Jednoduchý skript který z výstupu webu https://slovnik.seznam.cz vrací
jednoduchý textový výstup. Zároveň je přiložen jednoduchý vim skript pro
použití z vimu.

Je toho spousta, co by bylo možno přidat (pull requesty jsou vítány!)
... konfigurace pro jiné jazyky (v současné chvíli má skript zadrátováno
cs->en), moduly pro jiné editory (zejména Emacs a Sublime Text by 
byly vítány!), etc.
