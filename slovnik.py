#!/usr/bin/python3

import logging
import re
import sys
try:
    from urllib import urlencode
    from urllib2 import urlopen, URLError
except ImportError:
    from urllib.request import urlopen  # pylint: disable=E0611,F0401
    from urllib.parse import quote_plus, urlencode  # pylint: disable=E0611,F0401
    from urllib.error import URLError  # pylint: disable=E0611,F0401

import html2text
from bs4 import BeautifulSoup, NavigableString

logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger('slovnik-seznam')


def preview(word):
    # URL to get is https://slovnik.seznam.cz/api/slovnik?dictionary=en&query=přiživt
    # https://slovnik.seznam.cz/preklad/cesky_anglicky/obl%C3%ADben%C3%BD
    baseUrl = "http://slovnik.seznam.cz/preklad/cesky_anglicky"
    fromLanguage = "cz"
    toLanguage = "en"
    out = ''

    if fromLanguage == toLanguage:
        return word
    else:
        query = quote_plus(word)
        url = '{}/{}'.format(baseUrl, query)
        log.debug('url = %s', url)
        try:
            inf = urlopen(url)
        except URLError:
            logging.error('url %s cannot be opened', url)
        else:
            result = inf.read()
            inf.close()
            bs = BeautifulSoup(result, 'lxml')
            our_div = bs.find(class_='TranslatePage-content')
            log.debug('our_div = %s', our_div)
            if our_div is not None:
                for br in our_div.find_all('br'):
                    br.replace_with('$#$#$#')
                for br in our_div.find_all(['a', 'div', 'h2', 'p', 'span'],
                        class_=re.compile(r'(Box|Header)-(content|header).*')):
                    parent = br.find_parent()
                    new_content = parent.append(NavigableString('$#$#$#'))
                out = html2text.HTML2Text().handle(our_div.get_text())
                out = out.replace('\n', ' ').replace(' , ', ', ')
                out = re.sub(r'\s*(\$#\$#\$#)+\s*', '\n\n', out)
            return out


def main():
    strout = '\n' + preview(' '.join(sys.argv[1:]))
    print(strout)


if __name__ == "__main__":
    main()
